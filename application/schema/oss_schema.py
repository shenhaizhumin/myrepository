from pydantic import BaseModel, Field
from fastapi import Form, Query


class OssFileSchema(object):
    def __init__(self, name: str = Query(...), size: int = Query(...)):
        self.name = name
        self.size = size


class OssCallbackParamsSchema(BaseModel):
    name: str
    size: str

    class Config:
        orm_mode = True


class OssFileCallbackSchema(object):
    def __init__(self, data: str = Form(...), size: int = Form(...), file_path: str = Form(..., alias='filepath'),
                 bucket: str = Form(None)):
        self.data = data
        self.size = size
        self.file_path = file_path
        self.bucket = bucket
